<?php

namespace Drupal\Tests\content_editing_message\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test module.
 *
 * @group content_editing_message
 */
class CrudFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_editing_message',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->createContentType(['type' => 'article']);

    $account =
            $this->drupalCreateUser([
              'administer site configuration',
              'administer content editing messages',
            ]);

    $this->drupalLogin($account);
  }

  /**
   * Test access to configuration page.
   */
  public function testCanAccessContentMessages() {
    $this->drupalGet('/admin/config/content/messages');
    $this->assertSession()->pageTextContains('Content Editing Messages');
  }

}
