<?php

namespace Drupal\Tests\content_editing_message\Unit\Entity;

use Drupal\Tests\UnitTestCase;
use Drupal\content_editing_message\Entity\ContentEditingMessage;

/**
 * Tests for the message entity.
 *
 * @group content_editing_message
 *
 * @coversDefaultClass \Drupal\content_editing_message\Entity\ContentEditingMessage
 */
class ContentEditingMessageTest extends UnitTestCase {

  /**
   * Test fixture.
   *
   * @var \Drupal\content_editing_message\Entity\ContentEditingMessage
   */
  protected $message;

  /**
   * Test data for the fixture.
   *
   * @var array
   */
  protected static $data = [
    'id' => 'foo',
    'weight' => '1',
    'customWeight' => '99',
    'type' => '0',
    'subject' => 'A test message',
    'body' => [
      'value' => 'Test message body',
      'format' => 'test_format',
    ],
    'bundles' => [
      'article',
      'page',
    ],
    'roles' => [
      'administrator',
      'content_editor',
    ],
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->message = new ContentEditingMessage(static::$data, 'content_editing_message');
  }

  /**
   * @covers ::getTypeLabel
   */
  public function testGetTypeLabel() {
    $this->assertEquals('Info', $this->message->getTypeLabel());
  }

  /**
   * @covers ::getTypeClass
   */
  public function testGetTypeClass() {
    $this->assertEquals('info-content-message', $this->message->getTypeClass());
  }

  /**
   * @covers ::getType
   */
  public function testGetType() {
    $this->assertEquals('0', $this->message->getType());
  }

  /**
   * @covers ::getWeightLabel
   */
  public function testGetWeightLabel() {
    $this->assertEquals('Bottom', $this->message->getWeightLabel());
  }

  /**
   * @covers ::getWeight
   */
  public function testGetWeight() {
    $this->assertEquals(static::$data['weight'], $this->message->getWeight());
  }

  /**
   * @covers ::getBundles
   */
  public function testGetBundles() {
    $this->assertEquals(static::$data['bundles'], $this->message->getBundles());
  }

  /**
   * @covers ::getRoles
   */
  public function testGetRoles() {
    $this->assertEquals(static::$data['roles'], $this->message->getRoles());
  }

  /**
   * @covers ::getSubject
   */
  public function testGetSubjet() {
    $this->assertEquals(static::$data['subject'], $this->message->getSubject());
  }

  /**
   * @covers ::getMessage
   */
  public function testGetMessage() {
    $this->assertEquals(static::$data['body']['value'], $this->message->getMessage());
  }

  /**
   * @covers ::getMessageFormat
   */
  public function getMessageFormat() {
    $this->assertEquals(static::$data['body']['format'], $this->message->getMessageFormat());
  }

}
