# CONTENTS OF THIS FILE

 - Introduction
 - Requirements
 - Installation
 - Configuration
 - Maintainers


## INTRODUCTION

The Content Editing Message (content_editing_message) module allows you to add
multiple messages to content and media forms with multiple displays options.
The module has a configuration page in which this functionality can be applied
to one or more content and media types.

## REQUIREMENTS

This module requires no modules outside of Drupal core.

## INSTALLATION

 * Install as you would normally install a contributed Drupal module. Visit
   (https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules)
   for further information.


## CONFIGURATION

 - Configure the user permissions in
   Administration » Configuration » Content Authoring » Content Editing Message:

    - Select a subject and message.
    - Choose a display type.
    - Choose a display position.
    - Select which bundles should display this message.
    - Create Editing Message.


## MAINTAINERS

 - Nuno Ramos - [-nrzr-](https://www.drupal.org/u/nrzr)
 - Debora Antunes - [dgaspara](https://www.drupal.org/u/dgaspara)
 - João Marques - [joaomarques736](https://www.drupal.org/u/joaomarques736)
 - Nelson Alves - [nsalves](https://www.drupal.org/u/nsalves)
 - Paulo Carvalho - [pmaiacar](https://www.drupal.org/u/pmaiacar)
 - Ricardo Tenreiro - [ricardotenreiro](https://www.drupal.org/u/ricardotenreiro)
 - Sara Barroso - [sara_asb](https://www.drupal.org/u/sara_asb)
 - Tiago Velasques - [tmiguelv](https://www.drupal.org/u/tmiguelv)


## SPONSORS

This project has been sponsored by:

 - [everis]
   Multinational consulting firm providing business and strategy solutions,
   application development, maintenance, and outsourcing services. Being part
   of the NTT Data group enables everis to offer a wider range of solutions and
   services through increased capacity as well as technological, geographical,
   and financial resources.
