<?php

/**
 * @file
 * Content editing message module file.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

/**
 * Implements hook__form_node_form_alter().
 */
function content_editing_message_form_node_form_alter(&$form, $form_state, $form_id) {
  $entity = $form_state->getFormObject()->getEntity();
  $form = content_editing_message_process_entity($entity, $form);
}

/**
 * Implements hook__form_media_form_alter().
 */
function content_editing_message_form_media_form_alter(&$form, $form_state, $form_id) {
  $entity = $form_state->getFormObject()->getEntity();
  $form = content_editing_message_process_entity($entity, $form);
}

/**
 * {@inheritdoc}
 */
function content_editing_message_process_entity(EntityInterface $entity, &$form) {
  $messages = content_editing_message_get_messages($entity);
  if (!empty($messages)) {
    $form = content_editing_message_display_message($entity, $messages, $form);
  }
  return $form;
}

/**
 * Get all configured messages.
 */
function content_editing_message_get_messages(EntityInterface $entity) {
  $messages = [];

  $query = \Drupal::entityTypeManager()->getStorage('content_editing_message')->getQuery();
  $query->condition('status', 1);
  $query->accessCheck(FALSE);
  $ids = $query->execute();
  if (!empty($ids)) {
    $messages = \Drupal::entityTypeManager()
      ->getStorage('content_editing_message')
      ->loadMultiple($ids);
  }
  return $messages;
}

/**
 * {@inheritdoc}
 */
function content_editing_message_display_message(EntityInterface $entity, array $messages, &$form) {

  $user_roles = \Drupal::currentUser()->getRoles();
  foreach ($messages as $message) {
    $message_roles = $message->getRoles();
    if ($message_roles) {
      if (in_array($entity->bundle(), $message->getBundles(), TRUE) && array_intersect($user_roles, $message_roles)) {
        content_editing_message_append_message($message, $form);
      }
    }
    elseif (in_array($entity->bundle(), $message->getBundles(), TRUE)) {
      content_editing_message_append_message($message, $form);
    }
    $form['#attached']['library'][] = 'content_editing_message/content_message';
  }
  return $form;
}

/**
 * {@inheritdoc}
 */
function content_editing_message_append_message($message, &$form) {
  $options = [
    0 => !isset($form['title']["#weight"]) ? -999 : $form['title']["#weight"] - 1,
    1 => !isset($form['status']["#weight"]) ? 999 : $form['status']["#weight"] + 1,
  ];

  $form[$message->id()] = [
    '#type' => 'fieldset',
    '#title' => t('@title', ['@title' => $message->getSubject()]),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => $message->getWeight() == 2 ? $message->getCustomWeight() : $options[$message->getWeight()],
    '#attributes' => ['class' => [$message->getTypeClass()]],
    '#group' => 'content',
  ];

  $form[$message->id()]['text'] = [
    '#markup' => Markup::create($message->getMessage() ?? ''),
    '#group' => 'content',
  ];

  if (\Drupal::moduleHandler()->moduleExists('field_group') && !empty($message->getGroup())) {
    $form['#group_children'][$message->id()]['text'] = $message->getGroup();
    $form['#group_children'][$message->id()] = $message->getGroup();
    $form[$message->getGroup()][$message->id()]['text'] = $form[$message->id()]['text'];
    $form[$message->getGroup()][$message->id()] = $form[$message->id()];
    unset($form[$message->id()]['text']);
    unset($form[$message->id()]);
  }
}

/**
 * Implements hook_help().
 */
function content_editing_message_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.content_editing_message':
      $output = '';
      $output .= '<h2>' . t('About') . '</h2>';
      $output .= '<p>' . t('The Content Editing Message
      module allows you to add multiple configurable messages to <a href=":node_help"
      title="Node module help">node</a> and <a href=":media_help" title="Media"
      module help">media</a> entities forms with
      multiple displays options.
      The module has a configuration page in which this functionality can be applied
      to one (or more) content entities</a>.',
      [
        ':media_help' => Url::fromRoute('help.page', ['name' => 'media'])->toString(),
        ':node_help' => Url::fromRoute('help.page', ['name' => 'node'])->toString(),
      ])
      . '</p>';
      $output .= '<h2>' . t('Uses') . '</h2>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Creating messages') . '</dt>';
      $output .= '<dd>' . t('In order to create editing messages, the website
      must have at least one content or media type. You can set a new editing message in by:<ul>
      <li>Select a subject and message</li>
      <li>Choose a display type</li>
      <li>Choose a form display position</li>
      <li>Select which bundles should display this message</li>
      </ul>') . '</dd>';
      $output .= '<dt>' . t('Administering messages') . '</dt>';
      $output .= '<dd>' . t('The Content page lists your configured messages,
      allowing you add new, edit, delete or translate existing messages. In order
      to translate content, the website must have at least two <a href=":language-help">languages</a>.',
      [
        ':language-help' => Url::fromRoute('help.page', ['name' => 'language'])->toString(),
      ]) . '</dd>';
      $output .= '<dt>' . t('User permissions') . '</dt>';
      $output .= '<dd>' . t('The module makes a permission available for <i>Administer content editing messages</i>,
       which can be set by role on the <a href=":permissions">permissions page</a>.',
      [
        ':permissions' =>
        Url::fromRoute('user.admin_permissions',
        [],
        ['fragment' => 'module-node'])->toString(),
      ]) . '</dd>';
      $output .= '</dl>';
      return $output;
  }
}
