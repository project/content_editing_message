<?php

namespace Drupal\content_editing_message\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ContentEditingMessageFormBase.
 *
 * Typically, we need to build the same form for both adding a new entity,
 * and editing an existing entity. Instead of duplicating our form code,
 * we create a base class. Drupal never routes to this class directly,
 * but instead through the child classes of CContentEditingMessageAddForm
 * and ContentEditingMessageEditForm.
 *
 * @package Drupal\content_editing_message\Form
 *
 * @ingroup content_editing_message
 */
class ContentEditingMessageFormBase extends EntityForm {

  /**
   * Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Factory method for ContentEditingMessageFormBase.
   *
   * When Drupal builds this class it does not call the constructor directly.
   * Instead, it relies on this method to build the new object. Why? The class
   * constructor may take multiple arguments that are unknown to Drupal. The
   * create() method always takes one parameter -- the container. The purpose
   * of the create() method is twofold: It provides a standard way for Drupal
   * to construct the object, meanwhile it provides you a place to get needed
   * constructor parameters from the container.
   *
   * In this case, we ask the container for an entity query factory. We then
   * pass the factory to our class as a constructor parameter.
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->moduleHandler = $container->get('module_handler');
    return $instance;
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::form().
   *
   * Builds the entity add/edit form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   *
   * @return array
   *   An associative array containing the content_editing_message
   *   add/edit form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get anything we need from the base class.
    $form = parent::buildForm($form, $form_state);

    // Drupal provides the entity to us as a class variable. If this is an
    // existing entity, it will be populated with existing values as class
    // variables. If this is a new entity, it will be a new object with the
    // class of our entity. Drupal knows which class to call from the
    // annotation on our ContentEditingMessage class.
    /** @var \Drupal\content_editing_message\ContentEditingMessageInterface $content_editing_message */
    $content_editing_message = $this->entity;

    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Machine name'),
      '#default_value' => $content_editing_message->id(),
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'source' => ['label'],
      ],
      '#disabled' => !$content_editing_message->isNew(),
    ];

    $form['message'] = [
      '#type' => 'fieldset',
      '#title' => 'Message',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];

    $form['message']['subject'] = [
      '#type' => 'textfield',
      '#title' => 'Title',
      '#default_value' => $content_editing_message->getSubject(),
      '#group' => 'content',
    ];

    $form['message']['body'] = [
      '#type' => 'text_format',
      '#format' => $content_editing_message->getMessageFormat() ?: filter_default_format(),
      '#title' => $this->t('Message'),
      '#default_value' => $content_editing_message->getMessage(),
      '#group' => 'content',
    ];

    $form['type'] = [
      '#type' => 'radios',
      '#required' => TRUE,
      '#title' => $this->t('Display type'),
      '#maxlength' => 4,
      '#default_value' => $content_editing_message->getType(),
      '#options' => [
        0 => $this->t('Info'),
        1 => $this->t('Warning'),
        2 => $this->t('Error'),
        3 => $this->t('Plain'),
      ],
    ];

    if ($this->moduleHandler->moduleExists('field_group')) {
      $form['group'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Group (machine name)'),
        '#default_value' => $content_editing_message->getGroup(),
      ];
      $form['text'] = [
        '#markup' => $this->t('Example: "group_mytab"<br> Please note that if no group is chosen this message will appear outside of groups.'),
      ];
    }
    $form['weight'] = [
      '#type' => 'radios',
      '#title' => $this->t('Display position'),
      '#default_value' => $content_editing_message->getWeight(),
      '#options' => [
        0 => $this->t('Top'),
        1 => $this->t('Bottom'),
        2 => $this->t('Custom'),
      ],
    ];

    $form['customWeight'] = [
      '#type' => 'textfield',
      '#attributes' => [
        ' type' => 'number',
      ],
      '#title' => $this->t('Custom weight'),
      '#maxlength' => 4,
      '#default_value' => $content_editing_message->getCustomWeight(),
      '#states' => [
        'visible' => [
          ':input[name="weight"]' => ['value' => 2],
        ],
      ],
    ];

    $options = [
      'node' => [
        'readable' => 'Node',
        'type' => 'node_type',
      ],
      'media' => [
        'readable' => 'Media',
        'type' => 'media_type',
      ],
    ];
    $entities_options = [];
    foreach ($options as $entity) {
      $entity_types = $this->getTypes($entity['type']);
      foreach ($entity_types as $entity_type) {
        $entities_options[$entity_type->id()] = $entity_type->label();
      }
    }

    $form['bundles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Bundles'),
      '#options' => $entities_options,
      '#default_value' => $content_editing_message->getBundles() ?: [],
    ];

    $user_roles = [];
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    foreach ($roles as $role) {
      $user_roles[$role->id()] = $role->label();
    }

    $form['roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Roles'),
      '#options' => $user_roles,
      '#default_value' => $content_editing_message->getRoles() ?: [],
    ];

    // Return the form.
    return $form;
  }

  /**
   * Get existing entity types.
   */
  private function getTypes($type) {
    $types = [];
    if ($this->entityTypeManager->hasDefinition($type)) {
      foreach ($this->entityTypeManager->getStorage($type)->loadMultiple() as $instance) {
        $types[] = $instance;
      }
    }
    return $types;
  }

  /**
   * Checks for an existing content_editing_message.
   *
   * @param string|int $entity_id
   *   The entity ID.
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return bool
   *   TRUE if this format already exists, FALSE otherwise.
   */
  public function exists($entity_id, array $element, FormStateInterface $form_state) {
    // Use the query factory to build a new entity query.
    $query = $this->entityTypeManager->getStorage('content_editing_message')->getQuery();

    // Query the entity ID to see if its in use.
    $result = $query->condition('id', $element['#field_prefix'] . $entity_id)
      ->accessCheck(FALSE)
      ->execute();

    // We don't need to return the ID, only if it exists or not.
    return (bool) $result;
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::actions().
   *
   * To set the submit button text, we need to override actions().
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   *
   * @return array
   *   An array of supported actions for the current entity form.
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    // Get the basic actins from the base class.
    $actions = parent::actions($form, $form_state);

    // Change the submit button text.
    $actions['submit']['#value'] = $this->t('Save');

    // Return the result.
    return $actions;
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::save().
   *
   * Saves the entity. This is called after submit() has built the entity from
   * the form values. Do not override submit() as save() is the preferred
   * method for entity form controllers.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   */
  public function save(array $form, FormStateInterface $form_state) {
    // EntityForm provides us with the entity we're working on.
    $content_editing_message = $this->getEntity();
    // Drupal already populated the form values in the entity object. Each
    // form field was saved as a public variable in the entity class. PHP
    // allows Drupal to do this even if the method is not defined ahead of
    // time.
    $status = $content_editing_message->save();

    if ($status == SAVED_UPDATED) {
      // If we edited an existing entity...
      $this->messenger()->addStatus(
        $this->t(
          'Message <a href=":url">%label</a> has been updated.',
          [
            '%label' => $content_editing_message->label() ?? '',
            ':url' => $content_editing_message->toUrl('edit-form')->toString() ?? '',
          ]
        )
      );
      $this->logger('content_editing_message')
        ->notice('Message has been updated.', ['%label' => $content_editing_message->label()]);
    }
    else {
      // If we created a new entity...
      $this->messenger()->addStatus(
        $this->t(
          'Message <a href=":url">%label</a> has been added.',
          [
            '%label' => $content_editing_message->label() ?? '',
            ':url' => $content_editing_message->toUrl('edit-form')->toString() ?? '',
          ]
        )
      );
      $this->logger('content_editing_message')
        ->notice('Message has been added.', ['%label' => $content_editing_message->label()]);
    }

    // Redirect the user back to the listing route after the save operation.
    $form_state->setRedirect('entity.content_editing_message.collection');
    return $status;
  }

}
