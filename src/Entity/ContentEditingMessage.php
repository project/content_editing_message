<?php

namespace Drupal\content_editing_message\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\content_editing_message\ContentEditingMessageInterface;

/**
 * Defines the content_editing_message entity.
 *
 * @see http://previousnext.com.au/blog/understanding-drupal-8s-config-entities
 * @see annotation
 * @see Drupal\Core\Annotation\Translation
 *
 * @ingroup content_editing_message
 *
 * @ConfigEntityType(
 *   id = "content_editing_message",
 *   label = @Translation("Content Editing Message"),
 *   admin_permission = "administer content editing messages",
 *   handlers = {
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "list_builder" = "Drupal\content_editing_message\Controller\ContentEditingMessageListBuilder",
 *     "form" = {
 *       "add" = "Drupal\content_editing_message\Form\ContentEditingMessageAddForm",
 *       "edit" = "Drupal\content_editing_message\Form\ContentEditingMessageEditForm",
 *       "delete" = "Drupal\content_editing_message\Form\ContentEditingMessageDeleteForm",
 *     }
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "add-form" = "/admin/config/content/messages/add",
 *     "edit-form" = "/admin/config/content/messages/manage/{content_editing_message}",
 *     "delete-form" = "/admin/config/content/messages/manage/{content_editing_message}/delete",
 *     "collection" = "/admin/config/content/messages"
 *   },
 *   config_export = {
 *     "id",
 *     "weight",
 *     "customWeight",
 *     "group",
 *     "type",
 *     "bundles",
 *     "roles",
 *     "subject",
 *     "body",
 *   }
 * )
 */
class ContentEditingMessage extends ConfigEntityBase implements ContentEditingMessageInterface {

  /**
   * The message body value and format.
   *
   * @var array
   */
  public $body = [
    'value' => '',
    'format' => '',
  ];

  /**
   * The message subject.
   *
   * @var string
   */
  public $subject;

  /**
   * The weight of the message.
   *
   * @var string
   */
  public $weight;

  /**
   * The custom weight of the message.
   *
   * @var string
   */
  public $customWeight;

  /**
   * The field_group of the message.
   *
   * @var string
   */
  public $group;

  /**
   * Message type.
   *
   * @var string
   */
  public $type;

  /**
   * Show message in bundles.
   *
   * @var string[]
   */
  public $bundles;

  /**
   * The configured user roles to display the message.
   *
   * @var string[]
   */
  public $roles;

  /**
   * {@inheritdoc}
   */
  public function getTypeLabel() {
    $display_type = [
      0 => 'Info',
      1 => 'Warning',
      2 => 'Error',
      3 => 'Plain',
    ];
    return $display_type[$this->get('type')];
  }

  /**
   * {@inheritdoc}
   */
  public function getTypeClass() {
    $display_type = [
      0 => 'info-content-message',
      1 => 'warning-content-message',
      2 => 'error-content-message',
      3 => '',
    ];
    return $display_type[$this->get('type')];
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->get('type');
  }

  /**
   * {@inheritdoc}
   */
  public function getWeightLabel() {
    $weight_type = [
      0 => 'Top',
      1 => 'Bottom',
      2 => $this->get('customWeight'),
    ];
    if (!isset($weight_type[$this->get('weight')])) {
      return 'Top';
    }
    return $weight_type[$this->get('weight')];
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->get('weight');
  }

  /**
   * {@inheritdoc}
   */
  public function getCustomWeight() {
    return $this->get('customWeight');
  }

  /**
   * {@inheritdoc}
   */
  public function getBundles() {
    return $this->get('bundles');
  }

  /**
   * {@inheritdoc}
   */
  public function getRoles() {
    return $this->get('roles');
  }

  /**
   * {@inheritdoc}
   */
  public function getGroup() {
    return $this->get('group');
  }

  /**
   * {@inheritdoc}
   */
  public function getSubject() {
    return $this->get('subject');
  }

  /**
   * {@inheritdoc}
   */
  public function getMessage() {
    return $this->get('body')['value'];
  }

  /**
   * {@inheritdoc}
   */
  public function getMessageFormat() {
    return $this->get('body')['format'];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    if (!is_null($this->get('bundles'))) {
      $this->set('bundles', array_keys(array_filter($this->get('bundles'))));
    }

    if (!is_null($this->get('roles'))) {
      $this->set('roles', array_keys(array_filter($this->get('roles'))));
    }
    parent::preSave($storage);
  }

}
