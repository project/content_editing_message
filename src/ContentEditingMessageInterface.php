<?php

namespace Drupal\content_editing_message;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines a content editing message interface.
 */
interface ContentEditingMessageInterface extends ConfigEntityInterface {

  /**
   * Gets the type label.
   *
   * @return string
   *   The type bundle.
   */
  public function getTypeLabel();

  /**
   * Gets the type class.
   *
   * @return string
   *   The type bundle.
   */
  public function getTypeClass();

  /**
   * Gets the type id bundle.
   *
   * @return int
   *   The type bundle.
   */
  public function getType();

  /**
   * Gets the weight label were to display the message.
   *
   * @return string
   *   The weight label of the message.
   */
  public function getWeightLabel();

  /**
   * Gets the weight id were to display the message.
   *
   * @return string
   *   The weight of the message.
   */
  public function getWeight();

  /**
   * Gets the custom weight id were to display the message.
   *
   * @return string
   *   The weight of the message.
   */
  public function getCustomWeight();

  /**
   * Get the bundles for which to display the message.
   *
   * @return array
   *   The relevant bundles.
   */
  public function getBundles();

  /**
   * Get the roles for which to display the message.
   *
   * @return array
   *   The relevant roles.
   */
  public function getRoles();

  /**
   * Get the field_group for which to display the message.
   *
   * @return string[]
   *   The relevant bundles.
   */
  public function getGroup();

  /**
   * Gets the message subject.
   *
   * @return string
   *   The message subject.
   */
  public function getSubject();

  /**
   * Gets the message value.
   *
   * @return string
   *   The message body text.
   */
  public function getMessage();

  /**
   * Gets the message format.
   *
   * @return string
   *   The format to be used for the message body.
   */
  public function getMessageFormat();

}
